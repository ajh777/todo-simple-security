package com.ajh.spring.integration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.integration.config.EnableIntegration;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.integration.dsl.IntegrationFlows;
import org.springframework.integration.dsl.MessageChannels;

import com.ajh.spring.ToDo;

@Configuration
@EnableIntegration
public class ToDoIntegration {
	@Bean
	public DirectChannel inputChannel01() {
		return MessageChannels.direct().get();
	}

	@Bean
	public IntegrationFlow simpleFlow01() {
		return IntegrationFlows
				.from(inputChannel01())
				.filter(ToDo.class, ToDo::isCompleted)
				.transform(ToDo.class, todo -> todo.getDescription().toUpperCase())
				.handle(System.out::println)
				.get();
	}
}

package com.ajh.spring.integration;

import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.support.MessageBuilder;

import com.ajh.spring.ToDo;

@Configuration
public class ToDoConfig {
	@Bean
	public ApplicationRunner runner1(MessageChannel inputChannel01) {
		return args -> {
			inputChannel01.send(MessageBuilder.withPayload(new ToDo("buy milk today", true)).build());
		};
	}

	@Bean
	public ApplicationRunner runner2(MessageChannel inputChannel02) {
		return args -> {
			inputChannel02.send(MessageBuilder.withPayload(new ToDo("read book today", true)).build());
		};
	}

	@Bean
	public ApplicationRunner runner3(MessageChannel inputChannel03) {
		return args -> {
			inputChannel03.send(MessageBuilder.withPayload(new ToDo("make plan today", true)).build());
		};
	}

	@Bean
	public ApplicationRunner runner4(MessageChannel inputChannel04) {
//		return args -> {
//			inputChannel04.send(MessageBuilder.withPayload(new ToDo("write code today", true)).build());
//		};
		return new ApplicationRunner() {
			@Override
			public void run(ApplicationArguments args) throws Exception {
				inputChannel04.send(MessageBuilder.withPayload(new ToDo("write code today", true)).build());
			}
		};
	}
}

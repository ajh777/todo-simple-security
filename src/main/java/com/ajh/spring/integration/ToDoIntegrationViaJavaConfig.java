package com.ajh.spring.integration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.annotation.Filter;
import org.springframework.integration.annotation.MessageEndpoint;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.integration.annotation.Transformer;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.integration.config.EnableIntegration;
import org.springframework.integration.handler.LoggingHandler;
import org.springframework.messaging.MessageChannel;

import com.ajh.spring.ToDo;

@Configuration
@EnableIntegration
public class ToDoIntegrationViaJavaConfig {
	@Bean
	public MessageChannel inputChannel03() {
		return new DirectChannel();
	}

	@Bean
	public MessageChannel transformChannel03() {
		return new DirectChannel();
	}

	@Bean MessageChannel logChannel03() {
		return new DirectChannel();
	}

	@MessageEndpoint
	class SimpleFilter {
		@Filter(inputChannel="inputChannel03", outputChannel="transformChannel03")
		public boolean process(ToDo message) {
			return message.isCompleted();
		}
	}

	@MessageEndpoint
	class SimpleTransformer {
		@Transformer(inputChannel="transformChannel03", outputChannel="logChannel03")
		public String process(ToDo message) {
			return message.getDescription().toUpperCase();
		}
	}

	@Bean
	@ServiceActivator(inputChannel="logChannel03")
	public LoggingHandler logging() {
		LoggingHandler adapter = new LoggingHandler(LoggingHandler.Level.INFO);
		adapter.setLoggerName("ToDoIntegrationViaJavaConfig");
//		adapter.setLogExpressionString("headers.id + ': ' + payload");
		adapter.setLogExpressionString("payload");
		return adapter;
	}
}

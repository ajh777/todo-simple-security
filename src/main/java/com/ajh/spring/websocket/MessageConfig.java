package com.ajh.spring.websocket;

import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;
import org.springframework.web.socket.config.annotation.WebSocketMessageBrokerConfigurer;

import com.ajh.spring.ToDoProperties;

@Configuration
@EnableWebSocketMessageBroker
@EnableConfigurationProperties(ToDoProperties.class)
public class MessageConfig implements WebSocketMessageBrokerConfigurer {
	private ToDoProperties props;

	public MessageConfig(ToDoProperties props) {
		this.props = props;
	}

	/*
	 * This method registers the STOMP protocol; in this case, it registers the /stomp
	 * end point and uses the JavaScript library SockJS (https://github.com/sockjs).
	 * 
	 * */
	@Override
	public void registerStompEndpoints(StompEndpointRegistry registry) {
		registry.addEndpoint(props.getEndpoint()).setAllowedOrigins("*").withSockJS();
	}

	/*
	 * This method configures the message broker options. In this case, it 
	 * enables the broker in the /todo end point. This means that the clients 
	 * that want to use the WebSockets broker need to use the /todo to connect.
	 * */
	@Override
	public void configureMessageBroker(MessageBrokerRegistry registry) {
		registry.enableSimpleBroker(props.getBroker());
		registry.setApplicationDestinationPrefixes(props.getApp());
	}

}

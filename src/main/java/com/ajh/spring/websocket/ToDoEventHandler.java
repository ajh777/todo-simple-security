package com.ajh.spring.websocket;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.rest.core.annotation.HandleAfterCreate;
import org.springframework.data.rest.core.annotation.RepositoryEventHandler;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Component;

import com.ajh.spring.ToDo;
import com.ajh.spring.ToDoProperties;

@Component
@RepositoryEventHandler(ToDo.class)
public class ToDoEventHandler {
	private Logger logger = LoggerFactory.getLogger(ToDoEventHandler.class);
	private SimpMessagingTemplate simpMessagingTemplate;
	private ToDoProperties toDoProperties;

	public ToDoEventHandler(SimpMessagingTemplate simpMessagingTemplate, ToDoProperties toDoProperties) {
		this.simpMessagingTemplate = simpMessagingTemplate;
		this.toDoProperties = toDoProperties;
	}

	@HandleAfterCreate
	public void handleToDoSave(ToDo todo) {
		this.simpMessagingTemplate.convertAndSend(this.toDoProperties.getBroker() + "/new", todo);
		logger.info(">> Sending message to WS ws://todo/new - " + todo);
	}
}

package com.ajh.spring;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.servlet.PathRequest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {
	private PersonRepository personRepo;

	@Autowired
	public SecurityConfig(PersonRepository personRepo) {
		super();
		this.personRepo = personRepo;
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http
			.authorizeRequests()
				.requestMatchers(PathRequest.toStaticResources().atCommonLocations()).permitAll()
				.anyRequest().fullyAuthenticated()
//			.and().formLogin() // VERY IMPORTANT for enabling of default login page
			.and().formLogin().loginPage("/login").permitAll()
			.and().logout().logoutRequestMatcher(new AntPathRequestMatcher("/logout")).logoutSuccessUrl("/login")
			.and().csrf().ignoringAntMatchers("/h2-console/**", "/api/**") // Special case for CSRF
			.and().headers().frameOptions().sameOrigin() // For frames in h2-console
			.and().httpBasic() // Allow clients like curl to use the basic authentication mechanism, e.g. "curl localhost:8080/api/toDos -u andy:123"
		;
	}

	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
//		auth.inMemoryAuthentication()
//			.passwordEncoder(passwordEncoder())
//			.withUser("andy")
//			.password(passwordEncoder().encode("123"))
//			.roles("ADMIN", "USER");
		auth.userDetailsService(new ToDoUserDetailsService(this.personRepo, passwordEncoder()));
	}

	@Bean
	public BCryptPasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}
}

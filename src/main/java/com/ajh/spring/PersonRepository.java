package com.ajh.spring;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface PersonRepository extends CrudRepository<Person, String> {
	/*
	 * It declared a query-method findByEmailIgnoreCase with an email as the
	 * parameter (annotated by @Param). This syntax tells the Spring Data REST that it needs to
	 * implement these methods and create the SQL statement accordingly (this is based on
	 * the name and the fields in the domain class, in this case, the email field).
	 *
	 * E.g. curl http://127.0.0.1:8080/api/persons/search/findByEmailIgnoreCase?email=mark@gmail.com -u admin@gmail.com:admin
	 *
	 * */
	public Person findByEmailIgnoreCase(@Param("email") String email);
}

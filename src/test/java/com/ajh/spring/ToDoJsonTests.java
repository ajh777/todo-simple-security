package com.ajh.spring;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.File;
import java.nio.file.Files;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.json.JsonTest;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.core.io.ClassPathResource;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@JsonTest
public class ToDoJsonTests {
	@Autowired
	private JacksonTester<ToDo> json;

	@Test
	public void toDoSerializeTest() throws Exception {
		ToDo todo = new ToDo("Read a book", true);
		assertThat(this.json.write(todo))
			.isEqualToJson("../../../todo.json")
			.hasJsonPathStringValue("@.description")
			.extractingJsonPathStringValue("@.description").isEqualTo("Read a book");
	}

	@Test
	public void toDoDeserializeTest() throws Exception {
		File jsonFile = new ClassPathResource("todo.json").getFile();
		String jsonString = new String(Files.readAllBytes(jsonFile.toPath()));
		assertThat(this.json.parse(jsonString)).isEqualTo(new ToDo("Read a book", true));
	}
}
